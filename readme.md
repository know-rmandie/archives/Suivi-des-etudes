**Le [projet original][origin] est désormais hébergé ailleurs sur cette instance. Merci de mettre à jour vos liens et références dans vos dépôts locaux**

* dépôt du projet : [origin]
* bugs et commentaires : [issues]

[origin]:https://framagit.org/know-rmandie/AGENor
[issues]:https://framagit.org/know-rmandie/AGENor/issues
